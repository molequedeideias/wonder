package er.modern.directtoweb.components.header;

import org.apache.log4j.Logger;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOComponent;
import com.webobjects.eoaccess.EOEntity;

import er.extensions.localization.ERXLocalizedString;
import er.modern.directtoweb.components.header.ERMD2WSimpleHeader.Keys;

/**

 *
 * @d2wKey displayNameForEntity
 * @d2wKey headerString
 *
 * @author nlessa
 *
*/

public class ERMD2WHeaderWithTaskAndEntityName extends ERMD2WHeader {

	private static final Logger log = Logger.getLogger(ERMD2WHeaderWithTaskAndEntityName.class);


	public interface Keys extends ERMD2WHeader.Keys {

		public static String header = "header";
		public static String displayNameForEntity = "displayNameForEntity";
		public static String headerString = "headerString";
	}

	public ERMD2WHeaderWithTaskAndEntityName(WOContext context) {
		super(context);
	}

	@Override
	public String headerString() {
		if (d2wContext().valueForKey(Keys.headerString) != null) {
			log.debug("Header" + d2wContext().valueForKey(Keys.headerString));
			return (String) d2wContext().valueForKey(Keys.headerString);

		} else {
			EOEntity entity = d2wContext().entity();
			String task = (String) d2wContext().valueForKey("task");
			if (entity != null) {
				String pageConfiguration =(String) d2wContext().valueForKey("pageConfiguration");

				if (pageConfiguration != null && pageConfiguration.startsWith("Create"))
						task = "create";

				log.debug("Header" + task + " "
						+ entity.nameForExternalName(entity.name(), " ", true));
				return localizer().localizedStringForKeyWithDefault(
						Keys.header + "." + task)
						+ " "
						+ localizer().localizedStringForKeyWithDefault(
								(String) d2wContext().valueForKey(
										Keys.displayNameForEntity))
								.toLowerCase();
			} else {
				log.debug("Header" + d2wContext().task());
				return localizer().localizedStringForKeyWithDefault(
						Keys.header + "." + task);

			}
		}
	}
}