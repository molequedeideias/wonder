package er.directtoweb.assignments;

import org.apache.log4j.Logger;

import com.webobjects.directtoweb.D2WContext;
import com.webobjects.eocontrol.EOKeyValueUnarchiver;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation._NSUtilities;

import er.extensions.foundation.ERXProperties;
import er.extensions.foundation.ERXStringUtilities;
import er.extensions.localization.ERXLocalizer;

public class ERDValueFromPropertiesAssignment extends ERDAssignment {

	/**
	 * Do I need to update serialVersionUID? See section 5.6 <cite>Type Changes
	 * Affecting Serialization</cite> on page 51 of the <a
	 * href="http://java.sun.com/j2se/1.4/pdf/serial-spec.pdf">Java Object
	 * Serialization Spec</a>
	 */
	private static final long serialVersionUID = 1L;

    /** logging support */
    static final Logger log = Logger.getLogger(ERDValueFromPropertiesAssignment.class);


	public NSArray dependentKeys(String keyPath) {
		// TODO Auto-generated method stub
		return null;
	}

	public ERDValueFromPropertiesAssignment(EOKeyValueUnarchiver u) {
		super(u);
	}

	public ERDValueFromPropertiesAssignment(String key, Object value) {
		super(key, value);
	}

	public static Object decodeWithKeyValueUnarchiver(
			EOKeyValueUnarchiver unarchiver) {
		return new ERDValueFromPropertiesAssignment(unarchiver);
	}

	@Override
	public Object fire(D2WContext c) {
		String key = (String) value();
		if (log.isDebugEnabled()) {
			String value = (String) ERXProperties.stringForKey(key);
			log.debug("Fire for property \"" + key + "\": " + value);
		}
		return (String) ERXProperties.stringForKey(key);
	}
}
