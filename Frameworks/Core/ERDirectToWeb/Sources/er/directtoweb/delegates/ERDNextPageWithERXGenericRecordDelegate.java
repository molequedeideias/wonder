package er.directtoweb.delegates;

import java.io.Serializable;

import com.webobjects.appserver.WOComponent;
import com.webobjects.directtoweb.D2WContext;
import com.webobjects.directtoweb.D2WPage;
import com.webobjects.directtoweb.NextPageDelegate;

import er.extensions.eof.ERXGenericRecord;

public class ERDNextPageWithERXGenericRecordDelegate implements NextPageDelegate, Serializable {

	/* Keys
	 * pageName - The name of the next page
	 * object - The Object to set in the nextPage
	 * setMethod - The name of the method for takeValueForkey make the magic
	 *
	 */
	private static final long serialVersionUID = 1287276L;

	protected String _pageName;
	protected ERXGenericRecord _object;
	private String _setMethodName;

	public ERDNextPageWithERXGenericRecordDelegate(String pageName, ERXGenericRecord object, String setMethodName) {
		_pageName = pageName;
		_object = object;
		_setMethodName = setMethodName;

	}

	public WOComponent nextPage(WOComponent sender) {
		WOComponent target = sender.pageWithName(_pageName);
		target.takeValueForKey(((D2WPage)sender).d2wContext(), "localContext");
		target.takeValueForKey(_object, _setMethodName);
		return target;
	}
}
