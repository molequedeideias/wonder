/*jadclipse*/// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) radix(10) lradix(10)
// Source File Name:   EOEntity.java

package com.webobjects.eoaccess;

import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import er.extensions.logging.ERXLogger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


// Referenced classes of package com.webobjects.eoaccess:
//            EOAttribute, EORelationship, EOEntityIndex, EOStoredProcedure,
//            EOProperty, EOModel, EOEntityClassDescription, _EOExpressionArray,
//            EOJoin, EOPropertyListEncoding, EOAttributeNameComparator, EOEntityIndexComparator,
//            EORelationshipComparator, EOModelGroup, _EOStringUtil, EOSQLExpression,
//            EOQualifierSQLGeneration

public class ERXEntity extends EOEntity
{

	private static final Pattern NeededByEOFPattern = Pattern.compile( "\\QNeededByEOF\\E(\\d+)" );
    private volatile NSMutableDictionary _singleTableSubEntityDictionary;

	public ERXLogger logger = ERXLogger.getERXLogger(ERXEntity.class);



    /**
	 * Creates and returns a new ERXEntity.
	 */
	public ERXEntity() {
		super();
	}

	/**
	 * Creates and returns a new EOEntity initialized from the
	 * property list plist belonging to the EOModel owner.
	 * plist is dictionary containing only property list data
	 * types (that is, NSDictionary, NSArray, NSData, and String).
	 * This constructor is used by EOModeler when it reads in an
	 * EOModel from a file.
	 *
	 * @param plist - A dictionary of property list values from which to initialize the new EOEntity object.
	 * @param owner - The EOModel to which the newly created entity belongs.
	 *
	 * @see EOPropertyListEncoding#encodeIntoPropertyList(NSMutableDictionary propertyList)
	 * @see EOPropertyListEncoding#awakeWithPropertyList(NSDictionary propertyList)
	 */
	public ERXEntity(NSDictionary plist, Object owner) {
		super(plist, owner);
	}


	/**
	 * ldeck radar bug#6302622.
	 *
	 * <p>Relating two sub-entities in vertical inheritance can fail to resolve
	 *  the foreign key for inserts. i.e., NeededByEOF<index> was not dealt with.
	 *  The simple fix is to return the primary key attribute at the given index.
	 *
	 * @see com.webobjects.eoaccess.EOEntity#anyAttributeNamed(java.lang.String)
	 */
	@Override
	public EOAttribute anyAttributeNamed(String name) {
		Matcher matcher = null;
		EOAttribute result = super.anyAttributeNamed(name);
		if (result == null && name != null && (matcher = NeededByEOFPattern.matcher(name)).matches()) {
			int neededIndex = Integer.valueOf(matcher.group(1));
			if (neededIndex >= primaryKeyAttributeNames().count()) {
				throw new IllegalStateException("No matching primary key found for entity'" + name() + "' with attribute'" + name + "'");
			}
			result = primaryKeyAttributes().objectAtIndex(neededIndex);
		}
		return result;
	}

	/**
	 * @see com.webobjects.eoaccess.EOEntity#hasExternalName()
	 * @since 5.4.1
	 */
	@Override
	public boolean hasExternalName() {
		// (ldeck) radar://6592526 fix for 5.4.3 regression which assumed that any parent entity that is abstract has no external name!
		return externalName() != null && externalName().trim().length() > 0;
	}

	/**
	 * Sets the class description for the instance.
	 *
	 * @param classDescription - the EOClassDescription to associate with the receiver.
	 */
	public void setClassDescription(EOClassDescription classDescription) {
		_classDescription = classDescription;
	}

	/**
	 * Overridden through our bottleneck.
	 */
	@Override
	protected EOKeyGlobalID _globalIDWithoutTypeCoercion(Object[] values) {
		return ERXSingleValueID.globalIDWithEntityName(name(), values);
	}

	public NSArray<EOAttribute> classAttributes() {
		NSMutableArray<EOAttribute> found = new NSMutableArray<EOAttribute>();
		for (String name : (NSArray<String>)this.classPropertyNames()) {
			if (this.attributeNamed(name) != null)
				found.add(this.attributeNamed(name));
		}
		return found.immutableClone();
	}

    public EOQualifier restrictingQualifier()
    {
        return _restrictingQualifier;
    }

    public NSArray attributesToFetch()
    {
        return _attributesToFetch();
    }


    public void encodeIntoPropertyList(NSMutableDictionary result)
    {
    	super.encodeIntoPropertyList(result);
//        result.setObjectForKey(_name, "name");
//        if(isReadOnly())
//            result.setObjectForKey("Y", "isReadOnly");
//        if(cachesObjects())
//            result.setObjectForKey("Y", "cachesObjects");
//        if(isAbstractEntity())
//            result.setObjectForKey("Y", "isAbstractEntity");
//        NSArray tempArray;
//        if((tempArray = sharedObjectFetchSpecificationNames()) != null && tempArray.count() != 0)
//            result.setObjectForKey(tempArray, "sharedObjectFetchSpecificationNames");
//        if(_className != null)
//            result.setObjectForKey(_className, "className");
//        if(_externalName != null)
//            result.setObjectForKey(_externalName, "externalName");
//        if(_externalQuery != null)
//            result.setObjectForKey(_externalQuery, "externalQuery");
//        if(_restrictingQualifier != null)
//            result.setObjectForKey(_restrictingQualifier.toString(), "restrictingQualifier");
//        if(_userInfo != null)
//            result.setObjectForKey(_userInfo, "userInfo");
//        if(_internalInfo != null)
//            result.setObjectForKey(_internalInfo, "internalInfo");
//        NSArray attributes = attributes();
//        int c = attributes.count();
//        if(c > 0)
//        {
//            NSMutableArray array = new NSMutableArray(c);
//            for(int i = 0; i < c; i++)
//            {
//                NSMutableDictionary d = new NSMutableDictionary(32);
//                Object plist = attributes.objectAtIndex(i);
//                ((EOAttribute)plist).encodeIntoPropertyList(d);
//                array.addObject(d);
//            }
//
//            result.setObjectForKey(array, "attributes");
//        }
//        NSArray classProperties = classProperties();
//        c = classProperties.count();
//        if(c > 0)
//        {
//            NSMutableArray array = new NSMutableArray(c);
//            for(int i = 0; i < c; i++)
//            {
//                Object plist = ((EOProperty)classProperties.objectAtIndex(i)).name();
//                array.addObject(plist);
//            }
//
//            result.setObjectForKey(array, "classProperties");
//        }
//        NSArray attributesUsedForLocking = attributesUsedForLocking();
//        c = attributesUsedForLocking.count();
//        if(c > 0)
//        {
//            NSMutableArray array = new NSMutableArray(c);
//            for(int i = 0; i < c; i++)
//            {
//                Object plist = ((EOProperty)attributesUsedForLocking.objectAtIndex(i)).name();
//                array.addObject(plist);
//            }
//
//            result.setObjectForKey(array, "attributesUsedForLocking");
//        }
//        NSArray primaryKeyAttributeNames = primaryKeyAttributeNames();
//        c = primaryKeyAttributeNames.count();
//        if(c > 0)
//            result.setObjectForKey(primaryKeyAttributeNames, "primaryKeyAttributes");
//        NSArray rels = relationshipsPlist();
//        if(rels != null && rels.count() != 0)
//            result.setObjectForKey(rels, "relationships");
//        NSArray indexes = indexesPlist();
//        if(indexes != null && indexes.count() != 0)
//            result.setObjectForKey(indexes, "entityIndexes");
//        if(_storedProcedures != null)
//        {
//            c = _storedProcedures.count();
//            NSArray operations = _storedProcedures.allKeys();
//            NSMutableDictionary spNames = _storedProcedures.mutableClone();
//            for(int i = 0; i < c; i++)
//            {
//                String op = (String)operations.objectAtIndex(i);
//                spNames.setObjectForKey(((EOStoredProcedure)_storedProcedures.objectForKey(op)).name(), op);
//            }
//
//            result.setObjectForKey(spNames, "storedProcedureNames");
//        }
//        if(_batchCount != 0)
//            result.setObjectForKey(_NSUtilities.IntegerForInt(_batchCount), "maxNumberOfInstancesToBatchFetch");
//        if(_parent != null)
//            result.setObjectForKey(_parent.name(), "parent");
//        NSDictionary fetchSpecDict = _fetchSpecificationDictionary();
//        EOKeyValueArchiver archiver = new EOKeyValueArchiver();
//        String name;
//        for(Enumeration enumerator = fetchSpecDict.keyEnumerator(); enumerator.hasMoreElements(); archiver.encodeObject(fetchSpecDict.objectForKey(name), name))
//            name = (String)enumerator.nextElement();
//
//        NSDictionary d = archiver.dictionary();
//        if(d != null)
//            result.setObjectForKey(archiver.dictionary(), "fetchSpecificationDictionary");
    }


    protected String _singleTableSubEntityKey()
    {
        synchronized(EOModel._EOGlobalModelLock)
        {
            if(_singleTableSubEntityKey == null && _restrictingQualifier != null && (_restrictingQualifier instanceof EOKeyValueQualifier) && ((EOKeyValueQualifier)_restrictingQualifier).selector().equals(EOQualifier.QualifierOperatorEqual))
                _singleTableSubEntityKey = ((EOKeyValueQualifier)_restrictingQualifier).key();
        }
        return _singleTableSubEntityKey;
    }

    protected Object _subEntityKeyValue()
    {

        if(_restrictingQualifier == null)
            return null;
        Object value = ((EOKeyValueQualifier)_restrictingQualifier).value();
       if(value == null)
            return NSKeyValueCoding.NullValue;
   	//Tratar o caso de atributos que tem FactoryClass
       EOAttribute attr = attributeNamed(_singleTableSubEntityKey());
       logger.debug("Attr: " + attr);
       if (attr.valueFactoryMethodName() != null) {
    	   logger.debug("valueFactoryMethodName: " + attr.valueFactoryMethodName());
    	   Class clazz = _NSUtilities.classWithName(attr.className());
    	   Method methodo;
		try {
			methodo = clazz.getMethod(attr.valueFactoryMethodName(), Class.forName("java.lang.String"));
			logger.debug("Methodo: " + methodo.toString());
			logger.debug(methodo.invoke(null,value));
			return methodo.invoke(null,value);
		}
		catch (NoSuchMethodException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	   
    	   
   
       }
       return super._subEntityKeyValue();
//        if(value instanceof Number)
//        {
//            Number val = (Number)value;
//            EOAttribute attr = attributeNamed(_singleTableSubEntityKey());
//            switch(attr._valueTypeChar())
//            {
//            case 98: // 'b'
//                value = new Byte(val.byteValue());
//                break;
//
//            case 115: // 's'
//                value = new Short(val.shortValue());
//                break;
//
//            case 105: // 'i'
//                value = new Integer(val.intValue());
//                break;
//
//            case 108: // 'l'
//                value = new Long(val.longValue());
//                break;
//
//            case 102: // 'f'
//                value = new Float(val.floatValue());
//                break;
//
//            case 100: // 'd'
//                value = new Double(val.doubleValue());
//                break;
//
//            case 66: // 'B'
//                value = _NSUtilities.convertNumberIntoCompatibleValue(val, BigDecimal);
//                break;
//            }
//            return value;
//        } else
//        {
//            return value;
//        }
    }

    protected void _generateSingleTableSubEntityDictionary(NSMutableDictionary subEntityDict)
    {
        NSArray directSubEntities = subEntities();
        logger.debug("subEntities: " + directSubEntities);
        logger.debug("subEntityDict: " + subEntityDict);
        if(directSubEntities != null)
        {
            int cnt = directSubEntities.count();
            for(int i = 0; i < cnt; i++)
            {
                EOEntity sub = (EOEntity)directSubEntities.objectAtIndex(i);
                sub._generateSingleTableSubEntityDictionary(subEntityDict);
            }

        }
        Object keyValue = _subEntityKeyValue();
        if(keyValue != null)
            subEntityDict.setObjectForKey(this, keyValue);
        _singleTableSubEntityDictionary = subEntityDict;
    }

    protected NSDictionary _singleTableSubEntityDictionary()
    {
        if(!_flags_isSingleTableEntity)
            return null;
        synchronized(EOModel._EOGlobalModelLock)
        {
            if(_singleTableSubEntityDictionary == null)
            {
                NSMutableDictionary subDict = new NSMutableDictionary();
                _generateSingleTableSubEntityDictionary(subDict);
            }
        }
        return _singleTableSubEntityDictionary;
    }

    protected EOEntity _singleTableSubEntityForRow(NSDictionary row)
    {
        NSDictionary singleTableSubEntityDict = _singleTableSubEntityDictionary();
        logger.debug("Row: " + row);
        logger.debug("SingleTableSubEntityDict: " + singleTableSubEntityDict);

        if(singleTableSubEntityDict != null)
        {
            String key = _singleTableSubEntityKey();
            logger.debug("_singleTableSubEntityKey: " + key);

            if(key != null)
            {
                Object value = row.objectForKey(key);
                logger.debug("value: " + value);

                if(value != null) {
                	EOEntity  entity = (EOEntity)singleTableSubEntityDict.objectForKey(value);
                	logger.debug("entity" + entity);
                    return entity;
                }
            }
        }
        return null;
    }

    protected boolean _isSingleTableEntity()
    {
        return _flags_isSingleTableEntity;
    }

    protected EOQualifier _singleTableRestrictingQualifier()
    {
        synchronized(EOModel._EOGlobalModelLock)
        {
            if(_singleTableRestrictingQualifier == null)
            {
                EOQualifier qualifier = null;
                NSArray subEntities = subEntities();
                if(subEntities != null)
                {
                    int cnt = subEntities.count();
                    NSMutableArray subQuals = new NSMutableArray(cnt);
                    if(_restrictingQualifier != null)
                        subQuals.addObject(_restrictingQualifier);
                    for(int i = 0; i < cnt; i++)
                    {
                        EOQualifier qual = ((EOEntity)subEntities.objectAtIndex(i))._singleTableRestrictingQualifier();
                        if(qual != null)
                            subQuals.addObject(qual);
                    }

                    if(subQuals.count() > 0)
                        qualifier = new EOOrQualifier(subQuals);
                }
                _singleTableRestrictingQualifier = qualifier;
            }
        }
        return _singleTableRestrictingQualifier;
    }



    public void beautifyName()
    {
        setName(_EOStringUtil.nameForExternalNameSeparatorStringInitialCaps(name(), "_", true));
        NSArray atts = attributes();
        for(int i = atts.count(); i-- != 0;)
            ((EOAttribute)atts.objectAtIndex(i)).beautifyName();

        NSArray rels = relationships();
        for(int i = rels.count(); i-- != 0;)
            ((EORelationship)rels.objectAtIndex(i)).beautifyName();

    }

    public String valueForSQLExpression(EOSQLExpression context)
    {
        if(context == null)
            return _externalName;
        else
            return context.sqlStringForSchemaObjectName(_externalName);
    }

    protected NSArray _extraSingleTableAttributesToFetch(NSArray simpleAttributesToFetch)
    {
        NSMutableArray extraAttrs = new NSMutableArray();
        NSMutableArray myAttrNames = _NSArrayUtilities.resultsOfPerformingSelector(simpleAttributesToFetch, _NSArrayUtilities._nameSelector);
        NSArray subEntities = subEntities();
        if(subEntities != null)
        {
            int cnt = subEntities.count();
            for(int i = 0; i < cnt; i++)
            {
                EOEntity child = (EOEntity)subEntities.objectAtIndex(i);
                NSArray childAttrs = child.attributesToFetch();
                int m = myAttrNames.count();
                for(int c = childAttrs.count() - 1; c >= 0; c--)
                {
                    int comparison = -1;
                    EOAttribute childAttr = (EOAttribute)childAttrs.objectAtIndex(c);
                    if(m > 0)
                    {
                        String childAttrName = childAttr.name();
                        do
                        {
                            m--;
                            comparison = NSComparator._compareObjects(childAttrName, (Comparable)myAttrNames.objectAtIndex(m));
                        } while(comparison == -1 && m > 0);
                    }
                    if(comparison == 0)
                        continue;
                    extraAttrs.addObject(childAttr);
                    if(comparison == -1)
                    {
                        myAttrNames.insertObjectAtIndex(childAttr.name(), 0);
                    } else
                    {
                        m++;
                        myAttrNames.insertObjectAtIndex(childAttr.name(), m);
                    }
                }

            }

        }
        return extraAttrs;
    }

}