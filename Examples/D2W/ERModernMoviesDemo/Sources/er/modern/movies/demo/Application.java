package er.modern.movies.demo;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.foundation.NSArray;

import er.modern.movies.demo.JarResourceRequestHandler;

import er.extensions.appserver.ERXApplication;
import er.extensions.appserver.navigation.ERXNavigationManager;

public class Application extends ERXApplication {

	private static final Logger log = Logger.getLogger(Application.class);

	public static void main(String[] argv) {
		ERXApplication.main(argv, Application.class);
	}

	public Application() {
		ERXApplication.log.info("Welcome to " + name() + " !");
		setDefaultRequestHandler(requestHandlerForKey(directActionRequestHandlerKey()));

		if (isDirectConnectEnabled()) {
			registerRequestHandler(new JarResourceRequestHandler(), "_wr_");

			if (ERXApplication.isWO54()) {
				registerRequestHandler(new JarResourceRequestHandler(), "wr");
			}
		}

		NSArray models = EOModelGroup.defaultGroup().models();

		for (int i = 0; i < models.count(); i++) {
			EOModel model = (EOModel) models.objectAtIndex(i);

			log.debug("Dictionary " + model.name() + ": "
					+ model.connectionDictionary());
		}

	}

    @Override
    public void finishInitialization() {
    	super.finishInitialization();

    	// Setup main navigation
    	ERXNavigationManager.manager().configureNavigation();

    }
}
